#include <Arduino.h>
#include <ArduinoSTL.h>
#include <iterator>

#include "drivers.hpp"
#include "iterators.hpp"

Rgb rgb(Rgb::Pins { red: 3, green: 4, blue: 5 });
Button modeButton(2);
void modeButtonPressed() { modeButton.markPressed(); }

ArrayLoopIterator<char, 3> mode({ 'r', 'g', 'b' });

void setup() {
	Serial.begin(115200);

	rgb.setup();
	modeButton.setup(modeButtonPressed);

	while (!Serial) { delay(10); }
}

void update_rgb_color() {
	switch (*mode) {
		case 'r': rgb.set_color(Rgb::Color { red: 255, green: 0, blue: 0 }); break;
		case 'g': rgb.set_color(Rgb::Color { red: 0, green: 255, blue: 0 }); break;
		case 'b': rgb.set_color(Rgb::Color { red: 0, green: 0, blue: 255 }); break;
	}
}

void loop() {
	static DeltaCounter deltaCounter;
	static Countdown buttonTriggerCountdown(1000);

	update_rgb_color();

	auto delta = deltaCounter.delta_to(millis());

	if (modeButton.pressed()) {
		Serial.println("button pressed. waiting.");
		buttonTriggerCountdown.start();
	}

	if (buttonTriggerCountdown.tick(delta)) {
		++mode;
		update_rgb_color();
		Serial.print("mode changed to: ");
		Serial.println(*mode);
	}

	rgb.tick();

	delay(10);
}


