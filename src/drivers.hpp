#include <stdint.h>

#include "async.hpp"
#include "types.hpp"

/// simple controller for an RGB led
class Rgb {
	public:
		struct Pins {
			pin_number_t red;
			pin_number_t green;
			pin_number_t blue;
		};
		struct Color {
			uint8_t red;
			uint8_t green;
			uint8_t blue;
		};

	private:
		const Pins pins;
		// the initial value is unimportant as it should never be observed because `dirty`
		// defaults to `false` and the only way to change it is to call `set_color`
		const Color color = Color { red: 0, green: 0, blue: 0 };
		bool dirty = false;

	public:
		explicit Rgb(Pins pins) : pins(pins) {}

		void setup() const {
			pinMode(this->pins.red,   OUTPUT);
			pinMode(this->pins.green, OUTPUT);
			pinMode(this->pins.blue,  OUTPUT);
		}

		void set_color(Color color) {
			this->color = color;
			this->dirty = true;
		}

		/// update the output pins to match the last set color.
		///
		/// this method is separate from `set_color` to make it possible to call `set_color` from
		/// interrupts and/or batch updates in a performant way.
		void tick() {
			if (!this->dirty) return;

			digitalWrite(this->pins.red,   this->color.red);
			digitalWrite(this->pins.green, this->color.green);
			digitalWrite(this->pins.blue,  this->color.blue);

			this->dirty = false;
		}
};

/// generic motor controller for the Ardumoto motor shield
class Motor {
	public:
		enum class Direction { forward = 0, backward = 1 };

		struct Pins {
			pin_number_t dir;
			pin_number_t pwm;
		};

	private:
		const Pins pins;

	public:
		explicit Motor(Pins pins) : pins(pins) {}

		void setup() const {
			pinMode(this->pins.dir, OUTPUT);
			pinMode(this->pins.pwm, OUTPUT);
			digitalWrite(this->pins.dir, LOW);
			digitalWrite(this->pins.pwm, LOW);
		}

		void drive(Direction direction, pin_number_t speed) {
			digitalWrite(this->pins.dir, static_cast<pin_number_t>(direction));
			analogWrite(this->pins.pwm, speed);
		}

		void stop() {
			this->drive(Direction::forward, 0);
		}
};

/// a wrapper around Motor that allows to control the canon used in this project project
class Canon {
	using Direction = Motor::Direction;

	private:
		Motor motor;
		Countdown fireStopCountdown;
		Countdown reloadStopCountdown;

	public:
		explicit Canon(Motor motor) : motor(motor), fireStopCountdown(100), reloadStopCountdown(75) {}

		explicit Canon(Motor::Pins pins) : Canon(Motor(pins)) {}

		void setup() const {
			this->motor.setup();
		}

		void fire() {
			this->motor.drive(Direction::forward, 220);
			this->fireStopCountdown.start();
		}

		void reload() {
			this->motor.drive(Direction::backward, 100);
			this->reloadStopCountdown.start();
		}

		void tick(duration_t delta) {
			if (this->fireStopCountdown.tick(delta)) {
				this->motor.stop();
				this->reload();
			}
			if (this->reloadStopCountdown.tick(delta)) { this->motor.stop(); }
		}
};

/// controller for this project's semaphores
class Semaphore {
	public:
		enum class State { normal = 0, closed = 1 };
		enum class Led { white = 0, red_0 = 1, red_1 = 2, none = 3 };

		class Pins {
			public:
				pin_number_t white;
				pin_number_t red_0;
				pin_number_t red_1;

				pin_number_t pinOfLed(Led led) const {
					switch (led) {
						case Led::white: return this->white;
						case Led::red_0: return this->red_0;
						case Led::red_1: return this->red_1;
						default:         return -1;
					}
				}
		};

	private:
		Timer switchTimer;
		const Pins pins;
		State state = State::normal;
		Led activeLed;

	public:
		explicit Semaphore(duration_t switchLength, Pins pins) : switchTimer(switchLength), pins(pins) {}

		void setup() {
			pinMode(this->pins.white, OUTPUT);
			digitalWrite(this->pins.white, LOW);

			pinMode(this->pins.red_0, OUTPUT);
			digitalWrite(this->pins.red_0, LOW);

			pinMode(this->pins.red_1, OUTPUT);
			digitalWrite(this->pins.red_1, LOW);
		}

		void activeateLed(Led led) {
			if (this->activeLed != Led::none) digitalWrite(this->pins.pinOfLed(this->activeLed), LOW);
			if (led != Led::none) digitalWrite(this->pins.pinOfLed(led), HIGH);

			this->activeLed = led;
			this->switchTimer.reset();
		}

		State get_state() {
			return this->state;
		}

		void set_state(State state) {
			// if we weren't already in the desired state, activate the correct initial led for the new state
			if (this->state != state) {
				this->activeateLed(state == State::normal ? Led::white : Led::red_0);
			}
			this->state = state;
		}

		void tick(duration_t delta) {
			if (this->switchTimer.tick(delta)) {
				switch (this->state) {
					case State::normal:
						activeateLed(this->activeLed == Led::white ? Led::none : Led::white);
						break;
					case State::closed:
						activeateLed(this->activeLed == Led::red_0 ? Led::red_1 : Led::red_0);
						break;
				}
			}
		}
};

/// controller for reacting to button presses connected to interrupt pins
///
/// the buttons are expected to be connecting the target pin to ground, as they are in
/// `INPUT_PULLUP` mode.
class Button {
	private:
		const pin_number_t pin;
		// `triggered` is expected to be modified from an interrupt, so it needs to be marked as
		// volatile.
		volatile bool triggered;

	public:
		explicit Button(pin_number_t pin) : pin(pin) {}

		/// initialize the button to be ready for being polled.
		///
		/// \param handle
		/// a function pointer to the interrupt handler
		///
		/// since the `attachInterrupt` handler function reference must be valid for the static
		/// lifetime, we can not use a member function as the handler because:
		///
		/// a) the handler would need access to the class instance. this could be solved by
		///    providing a closure, but:
		/// b) the class might be instantiated in a non-static context.
		///
		/// consequentially, we have to force the user to supply their own function pointer that
		/// calls `markPressed` manually.
		void setup(void (*handle)(void)) {
			pinMode(this->pin, INPUT_PULLUP);
			attachInterrupt(digitalPinToInterrupt(this->pin), handle, RISING);
		}

		/// mark the button as pressed
		///
		/// this is intended to be called from the handle function passed to `setup`.
		void markPressed() {
			this->triggered = true;
		}

		/// return whether the button has been pressed and released since the last time this method
		/// has been called
		///
		/// this method returns true only once for each press and returns true even if the button is
		/// not currently pressed but it has been pressed since the last check.
		bool pressed() {
			if (this->triggered) {
				this->triggered = false;
				return true;
			}
			return false;
		}
};

/// controller for the <insert correct model> ultrasonic sensor
class DistanceSensor {
	public:
		struct Pins {
			pin_number_t ping;
			pin_number_t echo;
		};

	private:
		const Pins pins;

		/// converts the time between sending and receiving a ping to the distance travelled in
		/// centimetres assuming the speed it travelled at to be 29μs/cm
		static uint32_t microsecondsToCentimetres(uint32_t microseconds) {
			// the sound wave has to travel to the impact point and back and therefore needs to be
			// divided by 2
			return microseconds / 29 / 2;
		}

	public:
		explicit DistanceSensor(Pins pins) : pins(pins) {}

		/// initialize the sensor and make it ready to be polled.
		void setup() const {
			pinMode(this->pins.ping, OUTPUT);
			digitalWrite(this->pins.ping, LOW);
			pinMode(this->pins.echo, INPUT);
		}

		/// send a ping from the sensor's transmitter and return the number of microseconds it took
		/// to return to the receiver.
		duration_t read() const {
			digitalWrite(this->pins.ping, HIGH);
			delayMicroseconds(10);
			digitalWrite(this->pins.ping, LOW);

			return pulseIn(this->pins.echo, HIGH);
		}

		/// send a ping from the sensor's transmitter and return the number of centimetres from the
		/// impact point.
		///
		/// the calculation assumes the speed of sound in the current medium to be 29μs/cm
		uint32_t readCm() const {
			return microsecondsToCentimetres(this->read());
		};
};
