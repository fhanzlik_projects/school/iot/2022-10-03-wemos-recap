#include "types.hpp"

class DeltaCounter {
	private:
		instant_t last_time = 0;

	public:
		explicit DeltaCounter() {}

		duration_t delta_to(instant_t new_time) {
			// if we haven't initialised the last time yet, do it now.
			// note that while this branch also triggers if the last passed time was 0, this does
			// not have to concern us because unless the clock went backward.
			if (this->last_time == 0) {
				this->last_time = new_time;
			}

			auto delta = new_time - this->last_time;
			this->last_time = new_time;

			return delta;
		}
};

/// async timer for periodically running specific tasks
class Timer {
	private:
		instant_t time = 0;
		duration_t length;

	public:
		explicit Timer(duration_t length) : length(length) {}

		/// resets the timer to start timing again
		void reset() {
			this->time = 0;
		}

		/// adds the specified delta to the timer and return whether it has fired during this period
		bool tick(duration_t delta) {
			this->time += delta;
			if (this->time > this->length) {
				this->time = this->time % this->length;
				return true;
			}
			return false;
		}
};

/// async timer that only fires once after each activation
class Countdown {
	private:
		bool active = false;
		instant_t time = 0;
		duration_t length;

	public:
		explicit Countdown(duration_t length) : length(length) {}

		/// start the countdown if it wasn't active or restart it if it was.
		void start() {
			this->active = true;
			this->time = 0;
		}

		/// adds the specified delta to the timer and return whether it has fired during this period
		bool tick(duration_t delta) {
			if (!this->active) return false;

			this->time += delta;

			if (this->time > this->length) {
				this->active = false;

				return true;
			}
			return false;
		}
};
